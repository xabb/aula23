﻿Auxiliar de Muntatge i Manteniment d’Equips Informàtics

Aquest Programa de Formació i Inserció és una iniciació a la família professional d’Informàtica i comunicacions. L’alumne es prepara per a les funcions d’auxiliar de muntatge i manteniment de sistemes microinformàtics, perifèrics i xarxes de comunicació de dades, garantint el compliment de les diverses normatives en cada cas.

Sortides Professionals
	Operari en muntatge d’equips microinformàtics
	Auxiliar d’instal·lacions i reparacions d’equips telefònics i telegràfic
	Auxiliar d’instal·lació i reparació d’instal·lacions telefòniques
	Operari en manteniment de sistemes microinformàtics
	Auxiliar d’instal·lacions d’equips i sistemes de comunicació
	Auxiliar d’indústria de la producció i distribució d’energia elèctrica

===================================================================

Mòdul Professional: Instal·lació i manteniment de xarxes per a transmissió de dades.	
Codi: MPF3016.	
Durada: 130 hores 	
	
Aquest mòdul professional conté la formació associada a la funció d'instal·lar canalitzacions, cablatge i sistemes auxiliars en instal·lacions de xarxes locals en petits entorns.	
	
La definició d'aquesta funció inclou aspectes com:	
- La identificació de sistemes, elements, eines i mitjans auxiliars. 	
- El muntatge de les canalitzacions i suports. 	
- L'estesa de cables per a xarxes locals cablades. 	
- El muntatge dels elements de la xarxa local. 	
- La integració dels elements de la xarxa. 	
		
1. Selecciona els elements que configuren les xarxes per a la transmissió de veu i dades, descrivint les seves principals característiques i funcionalitat.	
	
	a) S'han identificat els tipus d'instal·lacions relacionades amb les xarxes de transmissió de veu i dades.
	b) S'han identificat els elements (canalitzacions, cablatges, antenes, armaris, «racks» i caixes, entre d'altres) d'una xarxa de transmissió de dades.
	c) S'han classificat els tipus de conductors (parell de coure, cable coaxial, fibra òptica, entre d'altres).
	d) S'ha determinat la tipologia de les diferents caixes (registres, armaris, «racks», caixes de superfície, d'encastar, entre d'altres).
	e) S'han descrit els tipus de fixacions (tacs, brides, cargols, femelles, grapes, entre d'altres) de canalitzacions i sistemes.
	f) S'han relacionat les fixacions amb l'element a subjectar.
	
2. Munta canalitzacions, suports i armaris en xarxes de transmissió de veu i dades, identificant-ne els elements en el pla de la instal·lació i aplicant tècniques de muntatge.	
	
	a) S'han seleccionat les tècniques i eines emprades per a la instal·lació de canalitzacions i la seva adaptació.
	b) S'han tingut en compte les fases típiques per al muntatge d'un «rack».
	c) S'han identificat en un croquis de l'edifici o part de l'edifici els llocs d'ubicació dels elements de la instal·lació.
	d) S'ha preparat la ubicació de caixes i canalitzacions.
	e) S'han preparat i mecanitzat les canalitzacions icaixes.
	f) S'han muntat els armaris («racks») interpretant el pla.
	g) S'han muntat canalitzacions, caixes i tubs, entre d'altres, assegurant la seva fixació mecànica.
	h) S'han aplicat normes de seguretat en l'ús d'eines i sistemes.
	
3. Desplega el cablatge d'una xarxa de veu i dades analitzant el seu traçat. 	
	
	a) S'han diferenciat els mitjans de transmissió emprats per a veu i dades.
	b) S'han reconegut els detalls del cablatge de la instal·lació i el seu desplegament (categoria del cablatge, espais pels quals discorre suport per a les canalitzacions, entre d'altres).
	c) S'han utilitzat els tipus de guies passafils, indicant la forma òptima de subjectar cables i guia.
	d) S'ha tallat i etiquetat el cable.
	e) S'han muntat els armaris de comunicacions i els seus accessoris.
	f) S'han muntat i connectat les preses d'usuari i plafons d'interconexió.
	g) S'ha treballat amb la qualitat i seguretat requerides.
	
4. Instal·la elements i sistemes de transmissió de veu i dades, tot reconeixent i aplicant les diferents tècniques de muntatge.	
	
	a) S'han acoblat els elements que constin de diverses peces. 
	b) S'han identificat el cablatge en funció del seu etiquetatge o colors. 
	c) S'han col·locat els sistemes o elements (antenes, amplificadors, entre d'altres) al seu lloc d'ubicació. 
	d) S'han seleccionat eines. e) S'han fixat els sistemes o elements. 
	f) S'ha connectat el cablatge amb els sistemes i elements, assegurant un bon contacte. 
	g) S'han col·locat els embellidors, tapes i elements decoratius. 
	h) S'han aplicat normes de seguretat, en l'ús d'eines i sistemes
	
5. Realitza operacions bàsiques de configuració en xarxes locals cablades relacionant-les amb les seves aplicacions.	
	
	a) S'han descrit els principis de funcionament de les xarxes locals. 
	b) S'han identificat els diferents tipus de xarxes i les seves estructures alternatives. 
	c) S'han reconegut els elements de la xarxa local identificant-los amb la seva funció. 
	d) S'han descrit els mitjans de transmissió. 
	e) S'ha interpretat el mapa físic de la xarxa local. 
	f) S'ha representat el mapa físic de la xarxa local. 
	g) S'han utilitzat aplicacions informàtiques per representar el mapa físic de la xarxa local. 
	
6. Compleix les normes de prevenció de riscos laborals i de protecció ambiental, identificant els riscos associats, les mesures i sistemes per prevenir-los. 	
	
	a) S'han identificat els riscos i el nivell de perillositat que suposen la manipulació dels materials, eines, útils, màquines i mitjans de transport. 
	b) S'han operat les màquines respectant les normes de seguretat. 
	c) S'han identificat les causes més freqüents d'accidents en la manipulació de materials, eines, màquines de tall i conformats, entre d'altres. 
	d) S'han descrit els elements de seguretat (proteccions, alarmes, passos d'emergència, entre d'altres) de les màquines i els sistemes de protecció individual (calçat, protecció ocular, indumentària, entre d'altres) que s'han d'emprar en les operacions de muntatge i manteniment. 
	e) S'ha relacionat la manipulació de materials, eines i màquines amb les mesures de seguretat i protecció personal requerits. 
	f) S'han identificat les possibles fonts de contaminació de l'entorn ambiental. 
	g) S'han classificat els residus generats per a la seva retirada selectiva. 
	h) S'ha valorat l'ordre i la neteja d'instal·lacions i sistemes com a primer factor de prevenció de riscos. 

===================================================================
Mòdul Professional: Muntatge i manteniment de sistemes i components informàtics.	
Codi: MPF3029	
	
Durada: 180 hores 	
	
Aquest mòdul professional conté la formació associada a la funció de muntar i mantenir sistemes i perifèrics microinformàtics, el seu emmagatzematge, etiquetatge i registre.	
	
La definició d'aquesta funció inclou aspectes com:	
- La identificació de components, eines, suports i perifèrics. 	
- El muntatge de sistemes i suports. 	
- La instal·lació del programari bàsic. 	
- La comprovació i manteniment de sistemes i perifèrics. 	
- L'emmagatzematge i trasllat de sistemes i components. 	
	
	
Resultats d'aprenentatge i criteris d'avaluació	
	
1. Selecciona els components i eines per a la realització del muntatge i manteniment de sistemes microinformàtics, descrivint-los i relacionant-los amb la seva funció i aplicació a la instal·lació.	
	
	a) S'han descrit les característiques dels elements elèctrics i electrònics utilitzats en el muntatge de sistemes.
	b) S'han descrit les operacions i comprovacions prèvies a la manipulació segura de components elèctrics i electrònics.
	c) S'han identificat els dispositius i eines necessaris en la manipulació segura de sistemes electrònics.
	d) S'han seleccionat les eines necessàries per al procediment de muntatge, substitució o connexió de components maquinari d'un sistema microinformàtic.
	e) S'han identificat funcionalment els components maquinari per a l'encadellat i manteniment d'un equip microinformàtic.
	f) S'han descrit les característiques tècniques de cada un dels components maquinari (interns i externs) utilitzats en el muntatge i manteniment d'un equip microinformàtic.
	g) S'han localitzat els blocs funcionals en plaques bases utilitzades en els sistemes microinformàtics.
	h) S'han identificat els tipus de ports, badies internes i cables de connexió (de dades i elèctrics, entre d'altres) existents d'un equip microinformàtic.
	i) S'han seguit les instruccions rebudes.
	
2. Acobla els components maquinari d'un equip microinformàtic, interpretant guies i instruccions i aplicant tècniques de muntatge.	
	
	a) S'ha comprovat cada component abans de la seva utilització, seguint les normes de seguretat establertes.
	b) S'han interpretat les guies d'instruccions referents als procediments d'integració o encadellat, substitució i connexió del component maquinari d'un sistema microinformàtic.
	c) S'han reconegut en diferents plaques de base cada un dels sòcols de connexió de microprocessadors i els dissipadors, entre d'altres.
	d) S'han acoblat els components maquinari interns (memòria, processador, targeta de vídeo, pila, entre d'altres) a la placa de base del sistema microinformàtic.
	e) S'ha fixat cada dispositiu o targeta a la ranura o badia corresponent, segons guies detallades d'instal·lació.
	f) S'han connectat adequadament aquells components maquinari interns (disc dur, DVD, CD-ROM, entre d'altres) que necessitin cables de connexió per a la seva integració en el sistema microinformàtic. 
	
3. Instal·la sistemes operatius monolloc identificant les fases del procés i relacionant-les amb la funcionalitat de la instal·lació.	
	
	a) S'han descrit els passos a seguir per a la instal·lació o actualització.
	b) S'ha verificat l'absència d'errors durant el procés de càrrega del sistema operatiu.
	c) S'han utilitzat les eines de control per a l'estructura de directoris i la gestió de permisos.
	d) S'han instal·lat actualitzacions i pegats del sistema operatiu segons les instruccions rebudes.
	e) S'han realitzat còpies de seguretat de les dadesf) S'han anotat les possibles errades produïdes en la fase d'arrencada de l'equip microinformàtic.
	g) S'han descrit les funcions de replicació física ("clonació") de discos i particions en sistemes microinformàtics.
	h) S'han utilitzat eines de programaria la instal·lació d'imatges de discos o particions assenyalant les restriccions d'aplicació de les mateixes.
	i) S'ha verificat la funcionalitat de la imatge instal·lada, tenint en compte el tipus de "clonació" realitzada.
	
4. Comprova la funcionalitat dels sistemes, suports i perifèrics instal·lats relacionant les intervencions amb els resultats a aconseguir.	
	
	a) S'ha aplicat a cada component maquinari i perifèric el procediment de testeig adequat.
	b) S'ha verificat que l'equip microinformàtic realitza el procediment d'encesa i de POST (Power On Self Test), identificant l'origen dels problemes, en el seu cas.
	c) S'ha comprovat la funcionalitat dels suports per a l'emmagatzemament d'informació.
	d) S'ha verificat la funcionalitat en la connexió entre components de l'equip microinformàtic i amb els perifèrics.
	e) S'han utilitzat eines de configuració, testeig i comprovació per verificar el funcionament del sistema.
	f) S'han utilitzat les eines i guies d'ús per comprovar l'estat dels suports i de la informació continguda en els mateixos.
	g) S'han registrat els resultats i les incidències produïdes en els processos de comprovació.
	
5. Realitza el manteniment bàsic de sistemes informàtics, suports i perifèrics, relacionant les intervencions amb els resultats que cal aconseguir.	
	
	a) S'ha comprovat per mitjà d'indicadors lluminosos, que els perifèrics connectats tenen alimentació elèctrica i les connexions de dades.
	b) S'han descrit els elements consumibles necessaris per ser utilitzats als perifèrics de sistemes microinformàtics.
	c) S'han utilitzat les guies tècniques detallades per substituir elements consumibles.
	d) S'han descrit les característiques dels components, dels suports i dels perifèrics per conèixer els aspectes que afectin el seu manteniment.
	e) S'han utilitzat les guies dels fabricants per identificar els procediments de neteja de components, suports i perifèrics.
	f) S'ha realitzat la neteja de components, suports i perifèrics respectant les disposicions tècniques establertes pel fabricant mantenint la seva funcionalitat.
	g) S'han recollit els residus i elements d'un sol ús de manera adequada per a la seva eliminació o reciclatge.
	
6. Emmagatzema equips, perifèrics i consumibles, descrivint les condicions de conservació i etiquetatge.	
	
	a) S'han descrit les condicions per manipular, transportar i emmagatzemar components i perifèrics d'un sistema microinformàtic.
	b) S'han identificat els tipus d'embalatge per al transport i emmagatzematge de cada dispositiu, perifèric i consumible.
	c) S'han utilitzat les eines necessàries per realitzar les tasques d'etiquetatge prèvies a l'embalatge i emmagatzemament de sistemes, perifèrics i consumibles.
	d) S'han utilitzat els mitjans auxiliars adequats als elements a transportar. 
	e) S'han aplicat les normes de seguretat en la manipulació i el transport d'elements i equips.
	f) S'ha comprovat que els components rebuts es corresponen amb l'albarà de lliurament i que es troben en bon estat.
	g) S'han registrat les operacions realitzades seguint els formats establerts.
	h) S'han recollit els elements d'un sol ús per a la seva eliminació o reciclatge.

===================================================================


Mòdul Professional: Operacions auxiliars per a la configuració i l'explotació
Codi: MPF3030	
Durada: 180 hores 	
	
Aquest mòdul professional conté la formació associada a la funció de configurar, utilitzar els components maquinari i els recursos bàsics d'un sistema microinformàtic.	
	
La definició d'aquesta funció inclou aspectes com:	
- La configuració dels paràmetres bàsics d'un sistema operatiu. 	
- La instal·lació de proteccions bàsiques. 	
- La gestió d'usuaris i recursos. 	
- L'ús de recursos compartits. 	
- La utilització de paquets ofimàtics. 	
- La configuració de navegadors i correu electrònic. 	
- La utilització de serveis d'Internet. 	
	
Resultats d'aprenentatge i criteris d'avaluació	
	
1. Selecciona els components i eines per a la realització del muntatge i manteniment de sistemes microinformàtics, descrivint-los i relacionant-los amb la seva funció i aplicació a la instal·lació.	
	
	a) S'han descrit les característiques dels elements elèctrics i electrònics utilitzats en el muntatge de sistemes.
	b) S'han descrit les operacions i comprovacions prèvies a la manipulació segura de components elèctrics i electrònics.
	c) S'han identificat els dispositius i eines necessaris en la manipulació segura de sistemes electrònics.
	d) S'han seleccionat les eines necessàries per al procediment de muntatge, substitució o connexió de components maquinari d'un sistema microinformàtic.
	e) S'han identificat funcionalment els components maquinari per a l'encadellat i manteniment d'un equip microinformàtic.
	f) S'han descrit les característiques tècniques de cada un dels components maquinari (interns i externs) utilitzats en el muntatge i manteniment d'un equip microinformàtic.
	g) S'han localitzat els blocs funcionals en plaques bases utilitzades en els sistemes microinformàtics.
	h) S'han identificat els tipus de ports, badies internes i cables de connexió (de dades i elèctrics, entre d'altres) existents d'un equip microinformàtic.
	i) S'han seguit les instruccions rebudes.
	
2. Acobla els components maquinari d'un equip microinformàtic, interpretant guies i instruccions i aplicant tècniques de muntatge.	
	
	a) S'ha comprovat cada component abans de la seva utilització, seguint les normes de seguretat establertes.
	b) S'han interpretat les guies d'instruccions referents als procediments d'integració o encadellat, substitució i connexió del component maquinari d'un sistema microinformàtic.
	c) S'han reconegut en diferents plaques de base cada un dels sòcols de connexió de microprocessadors i els dissipadors, entre d'altres.
	d) S'han acoblat els components maquinari interns (memòria, processador, targeta de vídeo, pila, entre d'altres) a la placa de base del sistema microinformàtic.
	e) S'ha fixat cada dispositiu o targeta a la ranura o badia corresponent, segons guies detallades d'instal·lació.
	f) S'han connectat adequadament aquells components maquinari interns (disc dur, DVD, CD-ROM, entre d'altres) que necessitin cables de connexió per a la seva integració en el sistema microinformàtic. 
	
3. Instal·la sistemes operatius monolloc identificant les fases del procés i relacionant-les amb la funcionalitat de la instal·lació.	
	
	a) S'han descrit els passos a seguir per a la instal·lació o actualització.
	b) S'ha verificat l'absència d'errors durant el procés de càrrega del sistema operatiu.
	c) S'han utilitzat les eines de control per a l'estructura de directoris i la gestió de permisos.
	d) S'han instal·lat actualitzacions i pegats del sistema operatiu segons les instruccions rebudes.
	e) S'han realitzat còpies de seguretat de les dades
	f) S'han anotat les possibles errades produïdes en la fase d'arrencada de l'equip microinformàtic.
	g) S'han descrit les funcions de replicació física ("clonació") de discos i particions en sistemes microinformàtics.
	h) S'han utilitzat eines de programari a la instal·lació d'imatges de discos o particions assenyalant les restriccions d'aplicació de les mateixes.
	i) S'ha verificat la funcionalitat de la imatge instal·lada, tenint en compte el tipus de "clonació" realitzada.
	
4. Comprova la funcionalitat dels sistemes, suports i perifèrics instal·lats relacionant les intervencions amb els resultats a aconseguir.	
	
	a) S'ha aplicat a cada component maquinari i perifèric el procediment de testeig adequat.
	b) S'ha verificat que l'equip microinformàtic realitza el procediment d'encesa i de POST (Power On Self Test), identificant l'origen dels problemes, en el seu cas.
	c) S'ha comprovat la funcionalitat dels suports per a l'emmagatzemament d'informació.
	d) S'ha verificat la funcionalitat en la connexió entre components de l'equip microinformàtic i amb els perifèrics.
	e) S'han utilitzat eines de configuració, testeig i comprovació per verificar el funcionament del sistema.
	f) S'han utilitzat les eines i guies d'ús per comprovar l'estat dels suports i de la informació continguda en els mateixos.
	g) S'han registrat els resultats i les incidències produïdes en els processos de comprovació.
	
5. Realitza el manteniment bàsic de sistemes informàtics, suports i perifèrics, relacionant les intervencions amb els resultats que cal aconseguir.	
	
	a) S'ha comprovat per mitjà d'indicadors lluminosos, que els perifèrics connectats tenen alimentació elèctrica i les connexions de dades.
	b) S'han descrit els elements consumibles necessaris per ser utilitzats als perifèrics de sistemes microinformàtics.
	c) S'han utilitzat les guies tècniques detallades per substituir elements consumibles.
	d) S'han descrit les característiques dels components, dels suports i dels perifèrics per conèixer els aspectes que afectin el seu manteniment.
	e) S'han utilitzat les guies dels fabricants per identificar els procediments de neteja de components, suports i perifèrics.
	f) S'ha realitzat la neteja de components, suports i perifèrics respectant les disposicions tècniques establertes pel fabricant mantenint la seva funcionalitat.
	g) S'han recollit els residus i elements d'un sol ús de manera adequada per a la seva eliminació o reciclatge.
	
6. Emmagatzema equips, perifèrics i consumibles, descrivint les condicions de conservació i etiquetatge.	
	
	a) S'han descrit les condicions per manipular, transportar i emmagatzemar components i perifèrics d'un sistema microinformàtic.
	b) S'han identificat els tipus d'embalatge per al transport i emmagatzematge de cada dispositiu, perifèric i consumible.
	c) S'han utilitzat les eines necessàries per realitzar les tasques d'etiquetatge prèvies a l'embalatge i emmagatzemament de sistemes, perifèrics i consumibles.
	d) S'han utilitzat els mitjans auxiliars adequats als elements a transportar. 
	e) S'han aplicat les normes de seguretat en la manipulació i el transport d'elements i equips.
	f) S'ha comprovat que els components rebuts es corresponen amb l'albarà de lliurament i que es troben en bon estat.
	g) S'han registrat les operacions realitzades seguint els formats establerts.h) S'han recollit els elements d'un sol ús per a la seva eliminació o reciclatge.

===================================================================
