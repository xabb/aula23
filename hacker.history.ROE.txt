Un poco de historia 
Los primeros hackers

La cultura hacker tiene su mítico origen en los años cincuenta. El Tech Model Railroad Club era (y sigue siendo) un club de estudiantes del prestigioso Massachusetts Institute of Technology  (MIT)  aficionados  a  las  maquetas  de  trenes.  Un grupo  de  miembros  del  TMRC  formaba  el  subcomité  de Signals  and  Power,  que  se  ocupaba  de  arreglar,  mejorar  y redistribuir los innumerables cables, interruptores, relés, etc., que  hacían  funcionar  el  complicado  circuito  que  tenían,  que  ocupaba  toda  una habitación. Dedicaban a esta tarea incontables horas, y con el tiempo fueron desarrollando su propia jerga: por ejemplo, llamaban hack a algo que se hacía no sólo por su (in)utilidad, sino también por el simple placer que suponía plantearse retos que exigían cierta innovación, estilo y técnica. 

Algunos de aquellos hackers tomaron una asignatura recién creada: programación de computadoras. Su profesor era el matemático John McCarthy, que acuñó el término «inteligencia artificial» e inventó el lenguaje de programación LISP. Inevitablemente, los hackers no tardaron en plantearse desafíos y poner en la programación la misma pasión que habían puesto en perfeccionar el circuito de trenes. Aquel  pequeño  grupo  de  hackers  dio  inadvertidamente  cuerpo  a una filosofía y ética propias: Se debe desconfiar de la autoridad y promover la descentralización.

Las burocracias crean reglas para consolidar el poder establecido, y ven el impulso constructivo de los hackers como una amenaza. La mejor manera de promover el libre intercambio de información son los sistemas abiertos, aquellos que no levantan fronteras artificiales entre el hacker y la información que necesita. Esto permite una mayor creatividad en general, y evita tener que reinventar la rueda una y otra vez.  La valía de un hacker debe juzgarse por sus hacks, no por criterios estúpidos como calificaciones académicas, edad, raza o posición. Un hacker puede crear arte y belleza con una computadora, pero no sólo en el resultado producido: el  propio  código  de  un  programa  puede  ser  bello,  si  está  escrito  con  maestría, es innovador y aprovecha al máximo los recursos disponibles. Además, las computadoras pueden mejorar nuestras vidas, incluso las de quienes no son hackers. Son herramientas poderosas con las que se puede hacer casi cualquier cosa que uno desee. 

Para los hackers, el trabajo y el dinero no son fines en sí mismos: el tiempo de ocio es más importante, y el dinero es básicamente un medio para poder dedicarse a actividades más afines a sus intereses personales o inquietudes intelectuales. Cuando trabajan en un hack, no es el dinero su principal motivación, sino la pasión de hacer algo interesante y creativo, y el reconocimiento del mismo por parte de los demás. Los resultados se ponen a libre disposición del resto de la comunidad, para  que  sean  criticados  o  mejorados  en  un  esfuerzo  colectivo  de  aprendizaje.

Defienden la libertad de expresión en la red, la privacidad, la libertad individual y el uso de la red como herramienta de denuncia y lucha contra situaciones de abuso d  injusticia  producidas  en  cualquier  lugar  del  mundo. Entienden  que  las  redes deben  ser  un  elemento  de  inclusión  y  entendimiento,  y  no  un  instrumento  que aumente las brechas sociales provocadas por la exclusión de personas y regiones en función de intereses políticos y económicos. 



FONT:
Reunión de Ovejas Electrónicas (ROE) p. 46
https://www.viruseditorial.net/paginas/pdf.php?pdf=ciberactivismo.pdf


